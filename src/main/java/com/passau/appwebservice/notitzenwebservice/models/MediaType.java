package com.passau.appwebservice.notitzenwebservice.models;

public enum MediaType {
    AUDIO,
    IMAGE
}

