package com.passau.appwebservice.notitzenwebservice.models;

import java.util.ArrayList;

public class IdList {
    ArrayList<String> ids;

    public IdList() {
    }

    public IdList(ArrayList<String> ids) {
        this.ids = ids;
    }

    public ArrayList<String> getIds() {
        return ids;
    }

    public void setIds(ArrayList<String> ids) {
        this.ids = ids;
    }
}
