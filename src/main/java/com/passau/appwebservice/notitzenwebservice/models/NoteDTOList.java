package com.passau.appwebservice.notitzenwebservice.models;

import java.util.ArrayList;

public class NoteDTOList {
    ArrayList<NoteDTO> noteDTOs;

    public NoteDTOList() {
    }

    public NoteDTOList(ArrayList<NoteDTO> noteDTOs) {
        this.noteDTOs = noteDTOs;
    }

    public ArrayList<NoteDTO> getNoteDTOs() {
        return noteDTOs;
    }

    public void setNoteDTOs(ArrayList<NoteDTO> noteDTOs) {
        this.noteDTOs = noteDTOs;
    }
}
