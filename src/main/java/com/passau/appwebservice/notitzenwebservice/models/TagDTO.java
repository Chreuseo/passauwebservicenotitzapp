package com.passau.appwebservice.notitzenwebservice.models;

import com.passau.appwebservice.notitzenwebservice.entity.TagEntity;

public class TagDTO {
    private String id;
    private String tagName;

    public TagDTO(String id, String tagName) {
        this.id = id;
        this.tagName = tagName;
    }

    public TagDTO(TagEntity tagEntity) {
        this.id = tagEntity.getId();
        this.tagName = tagEntity.getTagName();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
