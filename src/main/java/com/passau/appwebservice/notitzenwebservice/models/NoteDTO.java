package com.passau.appwebservice.notitzenwebservice.models;

import com.passau.appwebservice.notitzenwebservice.DatabaseService;
import com.passau.appwebservice.notitzenwebservice.entity.NoteEntity;
import com.passau.appwebservice.notitzenwebservice.entity.TagEntity;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class NoteDTO {
    private String id;
    private String author;
    private LocalDateTime lastChangeTimeStamp;
    private String headline;
    private String body;
    private boolean archived;
    private LocalDateTime dueDate;
    private List<TagDTO> tags = new ArrayList<>();

    public NoteDTO() {
    }

    public NoteDTO(String id, String author, LocalDateTime lastChangeTimeStamp, String headline, String body, boolean archived, LocalDateTime dueDate, List<TagDTO> tags) {
        this.id = id;
        this.author = author;
        this.lastChangeTimeStamp = lastChangeTimeStamp;
        this.headline = headline;
        this.body = body;
        this.archived = archived;
        this.dueDate = dueDate;
        this.tags = tags;
    }

    public NoteDTO(NoteEntity note, List<TagDTO> tags) {
        this.id = note.getId();
        this.author = note.getAuthor();
        this.lastChangeTimeStamp = note.getLastChangeTimeStamp();
        this.headline = note.getHeadline();
        this.body = note.getBody();
        this.archived = note.isArchived();
        this.dueDate = note.getDueDate();

        this.tags = tags;
    }

    public String getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public LocalDateTime getLastChangeTimeStamp() {
        return lastChangeTimeStamp;
    }

    public String getHeadline() {
        return headline;
    }

    public String getBody() {
        return body;
    }

    public boolean isArchived() {
        return archived;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public List<TagDTO> getTags() {
        return tags;
    }

    public void setTags(List<TagDTO> tags) {
        this.tags = tags;
    }
}
