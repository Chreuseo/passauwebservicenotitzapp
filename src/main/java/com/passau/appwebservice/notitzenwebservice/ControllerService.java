package com.passau.appwebservice.notitzenwebservice;

import com.passau.appwebservice.notitzenwebservice.entity.NoteEntity;
import com.passau.appwebservice.notitzenwebservice.entity.NoteTagsEntity;
import com.passau.appwebservice.notitzenwebservice.entity.TagEntity;
import com.passau.appwebservice.notitzenwebservice.models.NoteDTO;
import com.passau.appwebservice.notitzenwebservice.models.NoteDTOList;
import com.passau.appwebservice.notitzenwebservice.models.TagDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Component
@Service
public class ControllerService {
    @Autowired
    DatabaseService databaseService;

    public void deleteNotes(ArrayList<String> notes) {
        databaseService.baseNoteRepository.deleteAllByIdIn(notes);
    }

    public void saveNoteToDatabase(ArrayList<NoteDTO> notes) {
        ArrayList<NoteEntity> baseNotes = new ArrayList<>();
        ArrayList<TagEntity> tagEntities = new ArrayList<>();
        ArrayList<NoteTagsEntity> noteTagsEntities = new ArrayList<>();
        ArrayList<NoteTagsEntity> noteTagsToDelete = new ArrayList<>();

        notes.forEach(baseNoteDTO -> {
            baseNotes.add(new NoteEntity(baseNoteDTO));
            baseNoteDTO.getTags().forEach(tagDTO -> {
                tagEntities.add(new TagEntity(tagDTO));
                noteTagsEntities.add(new NoteTagsEntity(baseNoteDTO.getId(), tagDTO.getId()));
            });

            List<String> newTagIds = new ArrayList<>();
            baseNoteDTO.getTags().forEach(tagDTO -> {
                newTagIds.add(tagDTO.getId());
            });
            databaseService.noteTagRepository.findAllByNoteId(baseNoteDTO.getId()).forEach(noteTag ->{
                if(!newTagIds.contains(noteTag.getTagId())){
                    noteTagsToDelete.add(noteTag);
                }
            });
        });

        databaseService.baseNoteRepository.saveAll(baseNotes);
        databaseService.tagRepository.saveAll(tagEntities);
        databaseService.noteTagRepository.saveAll(noteTagsEntities);
        databaseService.noteTagRepository.deleteAll(noteTagsToDelete);
    }

    public List<TagDTO> getTagsByNoteId(String noteId){
        List<TagDTO> tagDTOs= new ArrayList<>();
        List<String> tagIds = new ArrayList<>();
        List<NoteTagsEntity> noteTagsEntities = databaseService.noteTagRepository.findAllByNoteId(noteId);
        for(NoteTagsEntity noteTagsEntity : noteTagsEntities){
            tagIds.add(noteTagsEntity.getTagId());
        }

        List<TagDTO> result = new ArrayList<>();
        ArrayList<TagEntity> tagEntities = databaseService.tagRepository.findByIdIn((ArrayList<String>) tagIds);
        tagEntities.forEach(tagEntity -> {
            result.add(new TagDTO(tagEntity));
        });

        return result;
    }

    public NoteDTOList getNotesById(ArrayList<String> ids) {
        ArrayList<NoteDTO> baseNotes = new ArrayList<>();

        databaseService.baseNoteRepository.findAllById(ids).forEach(baseNoteEntity -> {
            List<TagDTO> tagDTOs = getTagsByNoteId(baseNoteEntity.getId());
            baseNotes.add(new NoteDTO(baseNoteEntity, tagDTOs));
        });

        return new NoteDTOList(baseNotes);
    }

    public ArrayList<String> getNotesByUser(String mail) {
        return databaseService.getNoteIdsByUser(mail);
    }
}
