package com.passau.appwebservice.notitzenwebservice;

import com.passau.appwebservice.notitzenwebservice.entity.NoteEntity;
import com.passau.appwebservice.notitzenwebservice.entity.MediaObjectEntity;
import com.passau.appwebservice.notitzenwebservice.entity.TagEntity;
import com.passau.appwebservice.notitzenwebservice.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Component
public class DatabaseService {

    @Autowired
    public UserRepository userRepository;
    @Autowired
    public BaseNoteRepository baseNoteRepository;
    @Autowired
    public MediaObjectRepository mediaObjectRepository;
    @Autowired
    public TagRepository tagRepository;
    @Autowired
    public NoteTagRepository noteTagRepository;

    public void deleteNotes(ArrayList<String> noteIds) {
        baseNoteRepository.deleteAllByIdIn(noteIds);
    }

    public List<NoteEntity> getNotesByUser(String userMail) {
       return baseNoteRepository.findByAuthor(userMail);
    }

    public void addNotes(ArrayList<NoteEntity> notesToAdd) {
        baseNoteRepository.saveAll(notesToAdd);
    }

    public List<MediaObjectEntity> getAttachmentsByNoteId(String noteId) {
        return mediaObjectRepository.findByNoteId(noteId);
    }

    public void addTag(ArrayList<TagEntity> tags) {
        tagRepository.saveAll(tags);
    }

    public ArrayList<String> getNoteIdsByUser(String userEmail) {
        ArrayList<NoteEntity> list = baseNoteRepository.findByAuthor(userEmail);
        ArrayList<String> idList = new ArrayList<>();
        list.forEach(noteEntity -> {
            idList.add(noteEntity.getId());
        });
        return idList;
    }
}
