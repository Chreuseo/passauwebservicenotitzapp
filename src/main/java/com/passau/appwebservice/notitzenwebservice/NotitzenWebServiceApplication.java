package com.passau.appwebservice.notitzenwebservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotitzenWebServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NotitzenWebServiceApplication.class, args);
	}

}
