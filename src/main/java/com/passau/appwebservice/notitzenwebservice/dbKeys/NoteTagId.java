package com.passau.appwebservice.notitzenwebservice.dbKeys;

import java.io.Serializable;
import java.util.Objects;

public class NoteTagId implements Serializable {

    private String noteId;

    public String getNoteId() {
        return noteId;
    }

    public NoteTagId(String note_id, String tag_id) {
        this.noteId = note_id;
        this.tagId = tag_id;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    private String tagId;

    public NoteTagId() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NoteTagId noteTagId = (NoteTagId) o;
        return Objects.equals(noteId, noteTagId.noteId) && Objects.equals(tagId, noteTagId.tagId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(noteId, tagId);
    }
}
