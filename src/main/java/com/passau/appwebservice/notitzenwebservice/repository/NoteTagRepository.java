package com.passau.appwebservice.notitzenwebservice.repository;

import com.passau.appwebservice.notitzenwebservice.entity.NoteEntity;
import com.passau.appwebservice.notitzenwebservice.entity.NoteTagsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Component
public interface NoteTagRepository  extends JpaRepository<NoteTagsEntity, String> {

    List<NoteTagsEntity> findAllByNoteId(String noteId);
}
