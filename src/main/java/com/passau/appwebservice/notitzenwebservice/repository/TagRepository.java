package com.passau.appwebservice.notitzenwebservice.repository;

import com.passau.appwebservice.notitzenwebservice.entity.TagEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Repository
@Component
public interface TagRepository extends JpaRepository<TagEntity, String> {
    TagEntity getById(String id);

    ArrayList<TagEntity> findByIdIn(ArrayList<String> tags);
}
