package com.passau.appwebservice.notitzenwebservice.repository;

import com.passau.appwebservice.notitzenwebservice.entity.MediaObjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Component
public interface MediaObjectRepository extends JpaRepository<MediaObjectEntity, Long> {

    List<MediaObjectEntity> findByNoteId (String nodeId);
}
