package com.passau.appwebservice.notitzenwebservice.repository;

import com.passau.appwebservice.notitzenwebservice.entity.NoteEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
@Component
public interface BaseNoteRepository extends JpaRepository<NoteEntity, String> {

    ArrayList<NoteEntity> findByAuthor(String email);

    void deleteAllByIdIn(ArrayList<String> notes);
}
