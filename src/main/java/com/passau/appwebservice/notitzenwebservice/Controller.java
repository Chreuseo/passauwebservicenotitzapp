package com.passau.appwebservice.notitzenwebservice;

import com.passau.appwebservice.notitzenwebservice.entity.NoteEntity;
import com.passau.appwebservice.notitzenwebservice.models.IdList;
import com.passau.appwebservice.notitzenwebservice.models.NoteDTO;
import com.passau.appwebservice.notitzenwebservice.models.NoteDTOList;
import org.aspectj.weaver.ast.Not;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*", allowedHeaders = "*", exposedHeaders = "ACCESS_CONTROL_ALLOW_ORIGIN",
        methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.OPTIONS, RequestMethod.PUT })
@RestController
public class Controller {

    @Autowired
    DatabaseService databaseService;

    @Autowired
    ControllerService controllerService;

    @GetMapping("/getAllNotes")
    public ResponseEntity<Map<String, LocalDateTime>> getAllNotes(@RequestParam("userMail") String userMail) {
        Map<String, LocalDateTime> notes = new HashMap<>();
        List<NoteEntity> noteEntities = databaseService.getNotesByUser(userMail);
        for(NoteEntity n : noteEntities){
            notes.put(n.getId(), n.getLastChangeTimeStamp());
        }

        return new ResponseEntity<Map<String, LocalDateTime>>(notes, HttpStatus.OK);
    }

    @PostMapping("/getMissingNotes")
    public ResponseEntity<NoteDTOList> getMissingNotes(@RequestBody IdList missingNotes) {
        NoteDTOList notes = controllerService.getNotesById(missingNotes.getIds());
        return new ResponseEntity<NoteDTOList>(notes, HttpStatus.OK);
    }

    @PostMapping("/uploadNotes")
    public ResponseEntity<String> uploadNotes(@RequestBody NoteDTOList uploadedNotes) {
        controllerService.saveNoteToDatabase((ArrayList<NoteDTO>) uploadedNotes.getNoteDTOs());
        return new ResponseEntity<String>("Successfull upload", HttpStatus.OK);
    }

    @PostMapping("/deleteNotes")
    public ResponseEntity<String> deleteNotes(@RequestBody IdList notes) {
        controllerService.deleteNotes(notes.getIds());
        return new ResponseEntity<String>("Successfull deleted", HttpStatus.OK);
    }
}
