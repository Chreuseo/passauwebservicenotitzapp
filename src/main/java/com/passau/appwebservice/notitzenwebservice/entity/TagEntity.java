package com.passau.appwebservice.notitzenwebservice.entity;

import com.passau.appwebservice.notitzenwebservice.models.TagDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "TAG")
public class TagEntity {

    @Id
    @Column(name = "Id")
    private String id;

    @Column(name = "TagName")
    private String tagName;

    public TagEntity() {
    }

    public TagEntity(String id, String tagName) {
        this.id = id;
        this.tagName = tagName;
    }

    public TagEntity(TagDTO tagDTO) {
        this.id = tagDTO.getId();
        this.tagName = tagDTO.getTagName();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

}
