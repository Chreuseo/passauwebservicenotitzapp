package com.passau.appwebservice.notitzenwebservice.entity;

import com.passau.appwebservice.notitzenwebservice.models.NoteDTO;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "BASE_NOTE")
public class NoteEntity {
    @Id
    @Column(name = "Id")
    private String id;

    @Column(name = "author")
    private String author;

    @Column(name = "LastChange")
    private LocalDateTime lastChangeTimeStamp;

    @Column(name = "Headline")
    private String headline;

    @Lob                        //LargeObject -> no char limit
    @Column(name = "Body")
    private String body;

    @Column(name = "Archived")
    private boolean archived;

    @Column(name = "DueDate")
    private LocalDateTime dueDate;

    public NoteEntity() {

    }

    public NoteEntity(NoteDTO dto) {
        id = dto.getId();
        author = dto.getAuthor();
        lastChangeTimeStamp = dto.getLastChangeTimeStamp();
        headline = dto.getHeadline();
        body = dto.getBody();
        archived = dto.isArchived();
        dueDate = dto.getDueDate();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public LocalDateTime getLastChangeTimeStamp() {
        return lastChangeTimeStamp;
    }

    public void setLastChangeTimeStamp(LocalDateTime lastChangeTimeStamp) {
        this.lastChangeTimeStamp = lastChangeTimeStamp;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }
}
