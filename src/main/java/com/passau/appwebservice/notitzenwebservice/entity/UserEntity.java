package com.passau.appwebservice.notitzenwebservice.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "USER")
public class UserEntity {

    @Id
    @Column(name = "EmailAddress", unique = true)
    private String emailAddress;

    @OneToMany(mappedBy = "id")
    private List<NoteEntity> baseNotes;

    @Column(name = "RootPath")
    private String rootPath;

    @Column(name = "PW_HASH")
    private String pwHash;

    @Column(name = "lastSynchronized")
    private LocalDate lastSynchronized;

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public List<NoteEntity> getBaseNotes() {
        return baseNotes;
    }

    public void setBaseNotes(List<NoteEntity> baseNotes) {
        this.baseNotes = baseNotes;
    }

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public String getPwHash() {
        return pwHash;
    }

    public void setPwHash(String pwHash) {
        this.pwHash = pwHash;
    }

    public LocalDate getLastSynchronized() {
        return lastSynchronized;
    }

    public void setLastSynchronized(LocalDate lastSynchronized) {
        this.lastSynchronized = lastSynchronized;
    }
}
