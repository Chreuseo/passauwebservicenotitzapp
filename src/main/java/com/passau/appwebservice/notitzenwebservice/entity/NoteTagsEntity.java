package com.passau.appwebservice.notitzenwebservice.entity;

import com.passau.appwebservice.notitzenwebservice.dbKeys.NoteTagId;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@IdClass(NoteTagId.class)
@Table(name="NOTE_TAGS_ENTITY")
public class NoteTagsEntity implements Serializable {

    @Id
    @Column(name = "NoteId")
    private String noteId;

    @Id
    @Column(name = "TagId")
    private String tagId;

    public NoteTagsEntity() {

    }

    public NoteTagsEntity(String noteId, String tagId) {
        this.noteId = noteId;
        this.tagId = tagId;
    }

    public String getNoteId() {
        return noteId;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NoteTagsEntity that = (NoteTagsEntity) o;
        return noteId.equals(that.noteId) && tagId.equals(that.tagId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(noteId, tagId);
    }
}
