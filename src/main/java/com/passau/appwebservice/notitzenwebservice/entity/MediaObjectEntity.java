package com.passau.appwebservice.notitzenwebservice.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "MEDIA_OBJECT")
public class MediaObjectEntity {

    @Id
    @Getter
    @Setter
    @Column(name = "Id")
    private long id;

    @Getter
    @Setter
    @OneToOne(targetEntity = NoteEntity.class)
    private String noteId;

    @Getter
    @Setter
    @Column(name = "LocalPath")
    private String localPath;

    @Getter
    @Setter
    @Column(name = "MediaType")
    private String mediaType;
}
